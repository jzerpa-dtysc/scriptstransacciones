<?php

date_default_timezone_set('Europe/Madrid');

set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
include 'PHPExcel/IOFactory.php';

$target_path = "uploads/";
$target_path = $target_path . $_FILES['uploadedfile']['name'];

$input_format = "d-m-Y";
$output_format = "d/m/Y";

$option = $_POST['proyecto'];
$fecha_inicio = $_POST['fi'];
$fecha_fin = $_POST['ff'];
$xml = "";

$proyectos = array();

//Seleccion de cabecera
$proyectos['arte'] = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	<SolicitudModifTransacciones xmlns="http://ip6">
	  <detalleValidacionGasto>
      <codPrograma>EP</codPrograma>
      <codigoOperacion>0354_ARTE_6_E</codigoOperacion>
      <codSocio>404020</codSocio>
      <codTipoSocio>4</codTipoSocio>
      <tipoValidacion>P</tipoValidacion>
      <motivoDescertificacion></motivoDescertificacion>
      <observacionesDescertificacion></observacionesDescertificacion>
      <codControl></codControl>
      <fechaInicio>'. $fecha_inicio .'</fechaInicio>
      <fechaFin>'. $fecha_fin .'</fechaFin>
      <validacionFinal>N</validacionFinal>
      <verificacionInSitu>N</verificacionInSitu>
      <lugarDocumentacion>Lugar</lugarDocumentacion>
      <observaciones>Observaciones</observaciones>
      <transacciones>';

$proyectos['ete'] = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	<SolicitudModifTransacciones xmlns="http://ip6">
    <detalleValidacionGasto>
      <codPrograma>EP</codPrograma>
      <codigoOperacion>0384_ETE_6_E</codigoOperacion>
      <codSocio>437001</codSocio>
      <codTipoSocio>4</codTipoSocio>
      <tipoValidacion>P</tipoValidacion>
      <motivoDescertificacion/>
      <observacionesDescertificacion/>
      <fechaInicio>'. $fecha_inicio .'</fechaInicio>
      <fechaFin>'. $fecha_fin .'</fechaFin>
      <validacionFinal>N</validacionFinal>
      <verificacionInSitu>N</verificacionInSitu>
      <lugarDocumentacion>Lugar</lugarDocumentacion>
      <observaciones>Observaciones</observaciones>
      <transacciones>';

$proyectos['net'] = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	<SolicitudModifTransacciones xmlns="http://ip6">
    <detalleValidacionGasto>
      <codPrograma>EP</codPrograma>
      <codigoOperacion>0384_ETE_6_E</codigoOperacion>
      <codSocio>437001</codSocio>
      <codTipoSocio>4</codTipoSocio>
      <tipoValidacion>P</tipoValidacion>
      <motivoDescertificacion/>
      <observacionesDescertificacion/>
      <fechaInicio>'. $fecha_inicio .'</fechaInicio>
      <fechaFin>'. $fecha_fin .'</fechaFin>
      <validacionFinal>N</validacionFinal>
      <verificacionInSitu>N</verificacionInSitu>
      <lugarDocumentacion>Lugar</lugarDocumentacion>
      <observaciones>Observaciones</observaciones>
      <transacciones>';


$cabecera = $proyectos[$option];

if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {

	$nombre = "uploads/archivo.xml";
	$archivo= fopen($nombre, "w+");

	$inputFileName = $target_path;
	try {
		$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
	} catch(Exception $e) {
		die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
	}

	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	$transaccion = "";
	$first = TRUE;
        $contador=1;
	
	foreach($sheetData as $datos){
      if($datos['B'] == "")
         break;

      if ((!$first)){

          $date = DateTime::createFromFormat($input_format, $datos['F']);
          $datos['F'] = $date->format($output_format);
          $date = DateTime::createFromFormat($input_format, $datos['G']);
          $datos['G'] = $date->format($output_format);

          $valor1= $datos['H'];

          $var=array(".");
          $pagos1=str_replace($var,',',$datos['H']);

          $pago2= $datos['I'];
          $pagos2=str_replace($var,',',$pago2);

          $pago3= $datos['J'];
          $pagos3=str_replace($var,',',$pago3);

          $transaccion .='<transaccion>';
          $transaccion .=' <tipoTransaccion>'. $datos['A'] .'</tipoTransaccion>';
          $transaccion .=' <codActividad>'. $datos['B'] .'</codActividad>';
          $transaccion .=' <codTipoGasto>'. $datos['C'] .'</codTipoGasto>';
          $transaccion .=' <refDocumento>'. $datos['D'] .'</refDocumento>';
          $transaccion .=' <refContable>'. $datos['E'] .'</refContable>';
          $transaccion .=' <fechaRealizacion>'. $datos['F'] .'</fechaRealizacion>';
          $transaccion .=' <fechaPago>'. $datos['G'] .'</fechaPago>';
          $transaccion .=' <valorGastoConIVA>'. $pagos1 .'</valorGastoConIVA>';
          $transaccion .=' <valorGastoSinIVA>'. $pagos2 .'</valorGastoSinIVA>';
          $transaccion .=' <gastoAVerificar>'. $pagos3 .'</gastoAVerificar>';
          $transaccion .=' <regimenIVA>'. $datos['K'] .'</regimenIVA>';
          $transaccion .=' <tasaProrrataIVA>'. $datos['L'] .'</tasaProrrataIVA>';
          $transaccion .=' <proveedor>'. $datos['M'] .'</proveedor>';
          $transaccion .=' <cifProveedor>'. $datos['N'] .'</cifProveedor>';
          $transaccion .=' <descripcionGasto>'. $datos['O'] .'</descripcionGasto>';
          $transaccion .=' <codNut>'. $datos['P'] .'</codNut>';
          $transaccion .='</transaccion>';
      }
      $first = FALSE;

	}

	$xml .= $cabecera;
	$xml .= $transaccion;
	$xml .='</transacciones>
		</detalleValidacionGasto>
		</SolicitudModifTransacciones>';

	fwrite($archivo,$xml.PHP_EOL);
	fclose($archivo);

  $message = "XML Generado con exito.";
}
else{
    $message = "Ha ocurrido un error. :(";
}
?>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
  <!-- Optional theme -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <style>
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #eee;
    }

  </style>
</head>
<body>
  <div class="container">
    <p class="bg-success"><?php print $message; ?></p>
    <p>En la nueva ventana que se abrira dale a guardar y descarga el archivo en tu ordedanor</p>
    <a href='uploads/archivo.xml'  target='_blank' class="btn btn-success">Preview</a>
    <a href='uploads/archivo.xml' download="archivo.xml" target='_blank' class="btn btn-success">Download</a>
    <a href='index.php' class="btn btn-info">Back</a>
  </div> <!-- /container -->
<body>
</html>
