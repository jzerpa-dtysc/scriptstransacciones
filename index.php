<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
  <!-- Optional theme -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <style>
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #eee;
    }

  </style>
</head>
<body>
  <div class="container">

    <form role="form" enctype="multipart/form-data" action="uploader.php" method="POST">
      <div class="form-group">
        <div class="radio-inline">
          <label>
            <input type="radio" name="proyecto" id="optionsRadios1" value="arte" checked>
             ARTE
          </label>
        </div>

        <div class="radio-inline">
          <label>
            <input type="radio" name="proyecto" id="optionsRadios2" value="ete">
             ETE
          </label>
        </div>

        <div class="radio-inline">
          <label>
            <input type="radio" name="proyecto" id="optionsRadios3" value="net">
            NET
          </label>
        </div>
      </div>

      <div class="form-group">
        <label>Fecha Inicio:
          <input type="text" class="form-control fi" placeholder="Fecha Inicio" name="fi" value="01/03/2012">
        </label>
      </div>

      <div class="form-group">
        <label>Fecha Fin:
          <input type="text" class="form-control" placeholder="Fecha Fin" name="ff" value="31/01/2014">
        </label>
      </div>

      <div class="form-group">
        <label >
        Choose a file to upload:
          <input name="uploadedfile" type="file" />
        </label>
      </div>

      <button type="submit" class="btn btn-default">Generar XML</button>
    </form>
  </div> <!-- /container -->
<body>
</html>
